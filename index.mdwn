Green man

Encrypted e-mail
in Iceweasel

<div class="footer">
This book belongs to:
<br>
<br>
<hr />
</div>


---

---

## Project Debian Parl

Personal Laptop Computer

For secure email reading and sending only

Computer model: xxx

System: Debian Linux xxx

Admin: Jonas Smedegaard

2014

---

---

## Email account setup

Iceweasel

Imap

Ingoing and outgoing servers

---

---

## Making a gpg-key

Personal key

Choose a password

Password handling

Public key

Compiling

Terminal command

	/some command

This may take a while

---

---

## Exchanging public keys

Key signing

Ring of trust

---

---

## Sending encrypted and signed email

Signing your email

---

---

## Handling encrypted email

Receiving

Securing

---

---

## Contact info

Project manager: Erik Josefsson, EPFSUG, advisor of the Green Group, EP.

Project coordinator: Jonas Smedegaard, Debian, email: dr@jones.dk

User list: @parl.eu ?

Online summary:



<div class="footer">
<a href="http://helloearth.cc/">http://helloearth.cc/</a><br>
Programming: <a href="http://dr.jones.dk/">Jonas Smedegaard</a><br>
Design: <a href="http://sirireiter.dk/">Siri Reiter</a>
</div>
