htmlheader = templates/header.html
htmlfooter = templates/footer.html
scssfiles := $(wildcard styling/*.scss)
cssfiles = $(scssfiles:.scss=.css)

all: index.html
watch: csswatch

index.html: index.mdwn $(htmlheader) $(htmlfooter) $(cssfiles)
	cat $(htmlheader) > $@
	echo "<div id="book">" >> $@
	echo "<div id="page1" class="page"><div class="inner">" >> $@
	markdown $< >> $@
	perl -p0i -e '$$i or $$i++; s!(<hr\s*/?>\s*){2}(?{$$i++})!</div></div>\n\n<div id="page$$i" class="page"><div class="inner">!g' $@
	echo "</div></div>" >> $@
	cat $(htmlfooter) >> $@

$(cssfiles): %.css : %.scss
	scss --compass $< $@

csswatch: $(patsubst css/%.css,scss/%.scss,$(cssfiles)) $(sass_includedir)
	sass -I $(sass_includedir) --watch scss:css

clean:
	rm -rf .sass-cache
	rm -f index.html $(cssfiles)

.PHONY: all watch csswatch
